require 'sinatra'
require "net/http"
require 'json'

# Service Account Name - exotelrecording Key - 318265ff0ecc44a5917e64fa0eaf2926979e707e 
# API KEY NAME - EngFluenceSpeechAPIKey	 Key - AIzaSyBpaDvgNFQ5aZtM4ae_tMcrJjGmkLpnbns
# Project Name - engfluencecalls

# Bridge app

# http://localhost:4567/SpeechPracticeTopicClipPassthru?CallSid=e3b44bb6643f859165ff4f328ed2c211&From=07811000781&To=09243422233&Direction=incoming&DialCallDuration=0&StartTime=2016-07-07+15%3A47%3A20&EndTime=0000-00-00+00%3A00%3A00&CallType=call-attempt&DialWhomNumber=&Created=Thu%2C+07+Jul+2016+15%3A47%3A20&flow_id=101512&tenant_id=32785&CallFrom=07811000781&CallTo=08030752960&CurrentTime=2016-07-07+15%3A47%3A21
# https://engfluence-bridge.herokuapp.com/SpeechPracticeTopicClipPassthru?CallSid=e3b44bb6643f859165ff4f328ed2c211&From=07811000781&To=09243422233&Direction=incoming&DialCallDuration=0&StartTime=2016-07-07+15%3A47%3A20&EndTime=0000-00-00+00%3A00%3A00&CallType=call-attempt&DialWhomNumber=&Created=Thu%2C+07+Jul+2016+15%3A47%3A20&flow_id=101512&tenant_id=32785&CallFrom=07811000781&CallTo=08030752960&CurrentTime=2016-07-07+15%3A47%3A21
# https://people.zoho.com/people/api/forms/PlayClip/records?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&searchColumn=CallSid&searchValue=fe07c5cf0c96c6ae8c2e635ccfbff5b5

CREATOR_AUTH_TOKEN = "33693939990fd5488678e028f9c6c3fb"
ENGFL_CREATOR_AUTH_TOKEN = "7cf7901f3224c5f8d045a7372a923c6d"

before do
  content_type "text/plain"
end

get '/GetGFCallBack' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "GetGFCallBack"
  })
end

get '/GetSPCallBack' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "GetSPCallBack"
  })
end


get '/GetCallBack' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "GetCallBack"
  })
end


get '/ConversationClip' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "ConversationClip"
  })
end


get '/VM' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "VM"
  })
end


get '/WelcomeCallOver' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "WelcomeCallOver"
  })
end



get '/PostSampleReview' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PostSampleReview"
  })
end


get '/SeekingWelcomeCallBack' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "SeekingWelcomeCallBack"
  })
end


get '/PlayLessonClip' do
 logger.info "Inside PlayLessonClip"
  logger.info "params -> #{params}"
  
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PlayLessonClip"
  })
 logger.info "Zoho form response -> #{response.body}"
  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end


get '/ConnectingToExpert' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "ConnectingToExpert"
  })
end


get '/CallWithExpert' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "CallWithExpert"
  })
end


get '/ReviewTransfer' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "ReviewTransfer"
  })
end


get '/VoiceMail' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "VoiceMail"
  })
end


get '/WelcomeClipPlayed' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "WelcomeClipPlayed"
  })
end

get '/WelcomePassthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "WelcomePassthru"
  })
end


get '/SeekingCallBack' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "SeekingCallBack"
  })
end

post '/EndCall' do
  logger.info "Inside EndCall"
  logger.info "params -> #{params}"

  # Re-format times
  begin
    date_updated = Time.parse(params["DateUpdated"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    date_updated = nil
  end

  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/EndCall/record/add/")
  response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "Status" => params["Status"],
    "RecordingURL" => params["RecordingUrl"],
    "DateUpdated" => date_updated
  })

  logger.info "Zoho form response -> #{response.body}"
end

post '/Addv2PaymentLog' do

zoho_uri = URI.parse("https://creator.zoho.com/api/engfluence/json/user-module/form/Payment_Log/record/add/")
response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => ENGFL_CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "Name" => params["buyer_name"],
    "Email" => params["buyer"],
    "Phone" => params["buyer_phone"],
    "Amount" => params["amount"],
	  "Currency" => params["currency"],
    "fees" => params["fees"],
    "longurl" => params["longurl"],
    "mac" => params["mac"],
	  "payment_id" => params["paymentId"],
    "payment_request_id" => params["payment_request_id"],
    "purpose" => params["offerSlug"],
    "shorturl" => params["shorturl"],
	  "status" => params["status"],
	  "unitPrice"=> params["unitPrice"],
	  "offerTitle"=>params["offerTitle"],
	  "offerSlug"=>params["offerSlug"],
	  "quantity"=>params["quantity"],
    "Product_Name"=>"MonthlySub"
  })
  response.body
end


post '/AddGFPaymentLog' do

zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/Add_Payment_Log/record/add/")
response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "Name" => params["buyer_name"],
    "Email" => params["buyer"],
    "Phone" => params["buyer_phone"],
    "Amount" => params["amount"],
	  "Currency" => params["currency"],
    "fees" => params["fees"],
    "longurl" => params["longurl"],
    "mac" => params["mac"],
	  "payment_id" => params["paymentId"],
    "payment_request_id" => params["payment_request_id"],
    "purpose" => params["offerSlug"],
    "shorturl" => params["shorturl"],
	  "status" => params["status"],
	  "unitPrice"=> params["unitPrice"],
	  "offerTitle"=>params["offerTitle"],
	  "offerSlug"=>params["offerSlug"],
	  "quantity"=>params["quantity"],
    "Product_Name"=>"Grammar Foundation"
  })
  response.body
end

get '/StartLessonSessionPassthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "StartLessonSessionPassthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)


  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/PreviewIntroPassthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PreviewIntroPassthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/PlayQn1Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PlayQn1Passthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/PQ1Ans1Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ1Ans1Passthru"
  })
end

get '/PQ1Ans2Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ1Ans2Passthru"
  })
end

get '/PQ1Ans3Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ1Ans3Passthru"
  })
end

get '/PQ1Ans4Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ1Ans4Passthru"
  })
end

get '/PlayQn2Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PlayQn2Passthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/PQ2Ans1Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ2Ans1Passthru"
  })
end

get '/PQ2Ans2Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ2Ans2Passthru"
  })
end

get '/PQ2Ans3Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ2Ans3Passthru"
  })
end

get '/PQ2Ans4Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ2Ans4Passthru"
  })
end

get '/PlayQn3Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PlayQn3Passthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/PQ3Ans1Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ3Ans1Passthru"
  })
end

get '/PQ3Ans2Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ3Ans2Passthru"
  })
end

get '/PQ3Ans3Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ3Ans3Passthru"
  })
end

get '/PQ3Ans4Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ3Ans4Passthru"
  })
end

get '/PlayQn4Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PlayQn4Passthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/PQ4Ans1Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ4Ans1Passthru"
  })
end

get '/PQ4Ans2Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ4Ans2Passthru"
  })
end

get '/PQ4Ans3Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ4Ans3Passthru"
  })
end

get '/PQ4Ans4Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ4Ans4Passthru"
  })
end

get '/PlayQn5Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PlayQn5Passthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/PQ5Ans1Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ5Ans1Passthru"
  })
end

get '/PQ5Ans2Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ5Ans2Passthru"
  })
end

get '/PQ5Ans3Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ5Ans3Passthru"
  })
end

get '/PQ5Ans4Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PQ5Ans4Passthru"
  })
end

get '/ScoreClipPassthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "ScoreClipPassthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/LessonClipPassthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "LessonClipPassthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end


get '/ConclusionClipPassthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "ConclusionClipPassthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/PastLessonClipPassthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PastLessonClipPassthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end


get '/RQ1Ans1Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ1Ans1Passthru"
  })
end

get '/RQ1Ans2Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ1Ans2Passthru"
  })
end

get '/RQ1Ans3Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ1Ans3Passthru"
  })
end

get '/RQ1Ans4Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ1Ans4Passthru"
  })
end

get '/RQ2Ans1Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ2Ans1Passthru"
  })
end

get '/RQ2Ans2Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ2Ans2Passthru"
  })
end

get '/RQ2Ans3Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ2Ans3Passthru"
  })
end

get '/RQ2Ans4Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ2Ans4Passthru"
  })
end


get '/RQ3Ans1Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ3Ans1Passthru"
  })
end

get '/RQ3Ans2Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ3Ans2Passthru"
  })
end

get '/RQ3Ans3Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ3Ans3Passthru"
  })
end

get '/RQ3Ans4Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ3Ans4Passthru"
  })
end

get '/RQ4Ans1Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ4Ans1Passthru"
  })
end

get '/RQ4Ans2Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ4Ans2Passthru"
  })
end

get '/RQ4Ans3Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ4Ans3Passthru"
  })
end

get '/RQ4Ans4Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ4Ans4Passthru"
  })
end


get '/RQ5Ans1Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ5Ans1Passthru"
  })
end

get '/RQ5Ans2Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ5Ans2Passthru"
  })
end

get '/RQ5Ans3Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ5Ans3Passthru"
  })
end

get '/RQ5Ans4Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "RQ5Ans4Passthru"
  })
end



## PassThru requests
# Whenever you need info to flow from the telephony sys to your sys, you use passthru..
# it pushes IVR-entered digits, call recording etc.

get '/SpeechPracticeTopicClipPassthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "SpeechPracticeTopicClipPassthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/SpeechPracticePassthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "SpeechPracticePassthru"
  })

end

get '/ConvPracticeTopicClipPassthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "ConvPracticeTopicClipPassthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

post '/AddPaymentLog' do

zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/Add_Payment_Log/record/add/")
response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "Name" => params["buyer_name"],
    "Email" => params["buyer"],
    "Phone" => params["buyer_phone"],
    "Amount" => params["amount"],
	  "Currency" => params["currency"],
    "fees" => params["fees"],
    "longurl" => params["longurl"],
    "mac" => params["mac"],
	  "payment_id" => params["paymentId"],
    "payment_request_id" => params["payment_request_id"],
    "purpose" => params["offerSlug"],
    "shorturl" => params["shorturl"],
	  "status" => params["status"],
	  "unitPrice"=> params["unitPrice"],
	  "offerTitle"=>params["offerTitle"],
	  "offerSlug"=>params["offerSlug"],
	  "quantity"=>params["quantity"]

  })
  response.body
end


get '/AddLeadFromWebsite' do

zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/sales-app/form/Add_Lead_From_Website/record/add/")
response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "Name" => params["Name"],
    "Email" => params["Email"],
    "Phone" => params["Phone"],
    "Message" => params["Message"],
  })
  response.body
end

get '/DuringTheCallPassthru' do
  uri = URI.parse("http://posttestserver.com/post.php?dir=sridharraman")
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "DuringTheCallPassthru"
  })
  response.body
end

get '/ConversationPracticePassthru' do
  uri = URI.parse("http://posttestserver.com/post.php?dir=sridharraman")
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "ConversationPracticePassthru"
  })
  response.body
end

get '/ListeningPracticeClipPassthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "ListeningPracticeClipPassthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/ListeningQuestionClipPassthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "ListeningQuestionClipPassthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/ListeningAnswerPassthru' do
  uri = URI.parse("http://posttestserver.com/post.php?dir=sridharraman")
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "ListeningAnswerPassthru"
  })
  response.body

   call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end

end

get '/ListeningAnswerScorePassthru' do
  uri = URI.parse("http://posttestserver.com/post.php?dir=sridharraman")
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "ListeningAnswerScorePassthru"
  })
  response.body
   call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/ListeningAnswerSummaryPassthru' do
  uri = URI.parse("http://posttestserver.com/post.php?dir=sridharraman")
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "ListeningAnswerSummaryPassthru"
  })
  response.body
   call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/ReadingPracticeClipPassthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "ReadingPracticeClipPassthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/ReadingQuestionClipPassthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "ReadingQuestionClipPassthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/ReadingAnswerPassthru' do
  uri = URI.parse("http://posttestserver.com/post.php?dir=sridharraman")
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "ReadingAnswerPassthru"
  })
  response.body
   call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/ReadingAnswerScorePassthru' do
  uri = URI.parse("http://posttestserver.com/post.php?dir=sridharraman")
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "ReadingAnswerScorePassthru"
  })
  response.body
   call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/ReadingAnswerSummaryPassthru' do
  uri = URI.parse("http://posttestserver.com/post.php?dir=sridharraman")
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "ReadingAnswerSummaryPassthru"
  })
  response.body
   call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end


get '/LessonClipPassthru' do
  uri = URI.parse("http://posttestserver.com/post.php?dir=sridharraman")
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "LessonClipPassthru"
  })
  response.body
   call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end


get '/LessonDoubtPassthru' do
  uri = URI.parse("http://posttestserver.com/post.php?dir=sridharraman")
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "LessonDoubtPassthru"
  })
  response.body
end

get '/WordClipPassthru' do
  uri = URI.parse("http://posttestserver.com/post.php?dir=sridharraman")
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "WordClipPassthru"
  })
  response.body
   call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end


get '/WordDoubtPassthru' do
  uri = URI.parse("http://posttestserver.com/post.php?dir=sridharraman")
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "WordDoubtPassthru"
  })
  response.body
end


get '/EnglishDoubtPassthru' do
  uri = URI.parse("http://posttestserver.com/post.php?dir=sridharraman")
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "EnglishDoubtPassthru"
  })
  response.body
end

get '/SupportPassthru' do
  uri = URI.parse("http://posttestserver.com/post.php?dir=sridharraman")
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "SupportPassthru"
  })
  response.body
end

get '/SupportQueryRaisedPassthru' do
  uri = URI.parse("http://posttestserver.com/post.php?dir=sridharraman")
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "SupportQueryRaisedPassthru"
  })
  response.body
end

# This request fetches the audio clip to play
# Input:
##
# Output:
get '/FetchClipToPlay' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
  "digits" => params["digits"],
  "RecordingUrl" => params["RecordingUrl"],
  "CustomField" => params["CustomField"],
  "DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
  "PassthruType" => "SpeechPracticeTopicClipPassthru"
  })
  response.body
end




get '/PlayQn2Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PlayQn2Passthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/PostAns2Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PostAns2Passthru"
  })
end

get '/PlayQn3Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PlayQn3Passthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/PostAns3Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PostAns3Passthru"
  })
end

get '/PlayQn4Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PlayQn4Passthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/PostAns4Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PostAns4Passthru"
  })
end

get '/PlayQn5Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PlayQn5Passthru"
  })

  # TODO CHeck response status

  # Get audio clip based on user (CallSid)
  # sample_url = 'https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=f31ad5b2b1a8d251c48783d5d4d07944&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid="fe07c5cf0c96c6ae8c2e635ccfbff5b5")&raw=true'

  # sample_response = {"PlayClip"=>[{"Name"=>"Testing", "Phone"=>123456789, "PlayClip"=>"", "ID"=>"3108736000000323003", "PlayClipUrl"=>"<a href= \"http://www.google.com/audioclip\" target = \"_blank\">http://www.google.com/audioclip</a>", "CallSid"=>"fe07c5cf0c96c6ae8c2e635ccfbff5b5"}]}


  call_sid = params["CallSid"]
  # call_sid = "fe07c5cf0c96c6ae8c2e635ccfbff5b5"
  zoho_audioclip_fetch_uri = URI.parse("https://creator.zoho.com/api/json/exotelpassthru/view/PlayClip_Report?authtoken=#{CREATOR_AUTH_TOKEN}&scope=creatorapi&zc_ownername=varathkanth3&criteria=(CallSid==%22#{call_sid}%22)&raw=true")
  fetch_clip_response = Net::HTTP.get_response(zoho_audioclip_fetch_uri)

  # Parse response body and return audioclip url
  content_type "text/plain"

  begin
    audio_clip_record = JSON.parse(fetch_clip_response.body)
    return audio_clip_record["PlayClip"][0]["PlayClip"]
  rescue
    return nil
  end
end

get '/PostAns5Passthru' do
  zoho_uri = URI.parse("https://creator.zoho.com/api/varathkanth3/json/exotelpassthru/form/PassThru/record/add/")

  # Re-format times
  begin
    start_time = Time.parse(params["StartTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    start_time = nil
  end

  begin
    end_time = Time.parse(params["EndTime"]).strftime("%d-%b-%Y %H:%M:%S")
  rescue
    end_time = nil
  end

  begin
    current_time = Time.parse(params["CurrentTime"]).strftime("%d-%b-%Y %H:%M:%S")
    # current_time = Time.now.strftime("%d-%b-%Y %H:%M:%S")
  rescue
    current_time = nil
  end

  add_record_response = Net::HTTP.post_form(zoho_uri, {
    "authtoken" => CREATOR_AUTH_TOKEN,
    "scope" => "creatorapi",
    "CallSid" => params["CallSid"],
    "From" => params["From"],
    "To" => params["To"],
    "Direction" => params["Direction"],
    "DialCallDuration" => params["DialCallDuration"],
    "StartTime" => start_time,
    "CurrentTime" => current_time,
    "EndTime" => end_time,
    "CallType" => params["CallType"],
	"digits" => params["digits"],
	"RecordingUrl" => params["RecordingUrl"],
	"CustomField" => params["CustomField"],
	"DialWhomNumber" => params["DialWhomNumber"],
    "Created" => params["Created"],
    "flow_id" => params["flow_id"],
    "tenant_id" => params["tenant_id"],
    "CallFrom" => params["CallFrom"],
    "CallTo" => params["CallTo"],
	"PassthruType" => "PostAns5Passthru"
  })
end
